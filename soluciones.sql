﻿                                                /** MODULO I - UNIDAD II - CONSULTAS DE SELECCION 2 **/

USE ciclistas;

/* 1.1 Número de ciclistas que hay */
SELECT
  COUNT(*) nCiclistas
FROM
  ciclista c
;

/* 1.2 Número de ciclistas que hay del equipo Banesto */
SELECT
  COUNT(*) nCiclistasBANESTO
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO'
;

/* 1.3 La edad media de los ciclistas */
SELECT
  AVG(edad) mediaEdad
FROM
  ciclista c
;

/* 1.4 La edad media de los de equipo Banesto */
SELECT
  AVG(edad) mediaEdadBANESTO
FROM
  ciclista c
WHERE
  c.nomequipo LIKE 'BANESTO'
;

/* 1.5 La edad media de los ciclistas por cada Equipo */
SELECT
  c.nomequipo,
  AVG(c.edad) mediaEdad
FROM
  ciclista c
    GROUP BY c.nomequipo
;

/* 1.6 El número de ciclistas por equipo */
SELECT
  c.nomequipo,
  COUNT(*) nCiclistas
FROM
  ciclista c
    GROUP BY c.nomequipo
;

/* 1.7 El número total de puertos */
SELECT
  COUNT(*) nPuertos
FROM
  puerto p
;

/* 1.8 El número total de puertos mayores de 1500 */
SELECT
  COUNT(*) nPuertosSuperior1500
FROM
  puerto p
WHERE
  p.altura > 1500
;

/* 1.9 Listar el nombre de los equipos que tengan más de 4 ciclistas */
SELECT
  c1.nomequipo 
FROM
  (SELECT
     c.nomequipo,
     COUNT(*) nCiclistas
   FROM
     ciclista c
       GROUP BY c.nomequipo
       HAVING nCiclistas > 4) c1
;

/* 1.10 Listar el nombre de los equipos que tengasn más de 4 ciclistas
        cuya edad este entre 28 y 32 */
SELECT
  c1.nomequipo 
FROM
  (SELECT
    c.nomequipo,
    COUNT(*) nCiclistas
   FROM
    ciclista c
   WHERE
    c.edad BETWEEN 28 AND 32
      GROUP BY c.nomequipo
      HAVING nCiclistas > 4) c1
;

/* 1.11 Indícame el número de etapas que ha ganado cada uno de los ciclistas */
SELECT
  e.dorsal,
  COUNT(e.dorsal) nEtapasGANADAS
FROM
  etapa e
    GROUP BY e.dorsal
;

/* 1.12 Índicame el dorsal de los ciclistas que hayan ganado más de 1 etapa */
SELECT
  e.dorsal
FROM
  etapa e
    GROUP BY e.dorsal
    HAVING COUNT(*) > 1
;